package com.example.demo.service;

import com.example.demo.entity.Karyawan;
import com.example.demo.repository.KaryawanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KaryawanService {
    @Autowired
    private KaryawanRepository repository;

    public Karyawan saveKaryawan(Karyawan karyawan){
        return repository.save(karyawan);
    }

    public List<Karyawan> saveKaryawans(List<Karyawan> karyawans){
        return repository.saveAll(karyawans);
    }

    public List<Karyawan> getKaryawans(){
        return repository.findAll();
    }

    public Karyawan getKaryawanById(int id){
        return repository.findById(id).orElse(null);
    }

    public Karyawan getKaryawanByName(String nama){
        return repository.findByNama(nama);
    }

    public String deleteKaryawan(int id){
        repository.deleteById(id);
        return "Karyawan pensiun";
    }

    public Karyawan updateKaryawan(Karyawan karyawan){
        Karyawan existingKaryawan = repository.findById(karyawan.getId()).orElse(null);
        existingKaryawan.setNik(karyawan.getNik());
        existingKaryawan.setNama(karyawan.getNama());
        existingKaryawan.setTempatlahir(karyawan.getTempatlahir());
        existingKaryawan.setTanggallahir(karyawan.getTanggallahir());
        existingKaryawan.setJeniskelamin(karyawan.getJeniskelamin());
        existingKaryawan.setGoldarah(karyawan.getGoldarah());
        existingKaryawan.setAlamat(karyawan.getAlamat());
        existingKaryawan.setRtrw(karyawan.getRtrw());
        existingKaryawan.setKeldesa(karyawan.getKeldesa());
        existingKaryawan.setKecamatan(karyawan.getKecamatan());
        existingKaryawan.setAgama(karyawan.getAgama());
        existingKaryawan.setStatusperkawinan(karyawan.getStatusperkawinan());
        existingKaryawan.setPekerjaan(karyawan.getPekerjaan());
        existingKaryawan.setKewarganegaraan(karyawan.getKewarganegaraan());
        return repository.save(existingKaryawan);
    }

}
