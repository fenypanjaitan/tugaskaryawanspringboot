package com.example.demo.controller;

import com.example.demo.entity.Karyawan;
import com.example.demo.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins="*", allowedHeaders = "*")
@RestController
public class KaryawanController {
    @Autowired
    private KaryawanService service;

    @PostMapping("/addKaryawan")
    public Karyawan addKaryawan(@RequestBody Karyawan karyawan){
        return service.saveKaryawan(karyawan);
    }

    @PostMapping("/addKaryawans")
    public List<Karyawan> addKaryawans(@RequestBody List<Karyawan> karyawans){
        return service.saveKaryawans(karyawans);
    }

    @GetMapping("/karyawans")
    public List<Karyawan> findAllKaryawans(){
        return service.getKaryawans();
    }


    @GetMapping("/karyawans/{id}")
    public Karyawan findKaryawanById(@PathVariable int id){
        return service.getKaryawanById(id);
    }

    @GetMapping("/karyawanByNama/{nama}")
    public Karyawan findKaryawanByName(@PathVariable String nama){
        return service.getKaryawanByName(nama);
    }

    @PutMapping("/update/{id}")
    public void updateKaryawan(@RequestBody Karyawan karyawan, @PathVariable int id){
        karyawan.setId(id);
        service.updateKaryawan(karyawan);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteKaryawan(@PathVariable int id){
        return service.deleteKaryawan(id);
    }

}
