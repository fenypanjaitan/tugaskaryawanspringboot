-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2020 at 04:10 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `karyawan`
--

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(17);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan_master`
--

CREATE TABLE `karyawan_master` (
  `id` int(11) NOT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `goldarah` varchar(255) DEFAULT NULL,
  `jeniskelamin` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `keldesa` varchar(255) DEFAULT NULL,
  `kewarganegaraan` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `rtrw` varchar(255) DEFAULT NULL,
  `statusperkawinan` varchar(255) DEFAULT NULL,
  `tanggallahir` varchar(255) DEFAULT NULL,
  `tempatlahir` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `karyawan_master`
--

INSERT INTO `karyawan_master` (`id`, `agama`, `alamat`, `goldarah`, `jeniskelamin`, `kecamatan`, `keldesa`, `kewarganegaraan`, `nama`, `nik`, `pekerjaan`, `rtrw`, `statusperkawinan`, `tanggallahir`, `tempatlahir`) VALUES
(9, 'Katholik', 'kelapa dua', 'AB', 'Perempuan', 'kelapa dua', 'curug', 'Indonesia', 'marta', '123456890001122', 'Karyawan', '01/02', 'Belum Kawin', '2012-12-31', 'indonesia'),
(2, 'Ateis', 'kampung india', 'O', 'Laki-laki', 'peru', 'lima', 'Perancis', 'charlie', '1234567890000121', 'Karyawan', '02/02', 'Belum Kawin', '1999-11-11', 'indonesia'),
(3, 'Hindu', 'kampung india', 'B', 'Perempuan', 'india', 'kota india', 'India', 'delta', '1234567890000190', 'Karyawan', '01/02', 'Belum Kawin', '1998-01-12', 'india'),
(10, 'Konghuchu', 'kelapa dua', 'O', 'Laki-laki', 'kelapa dua', 'curug', 'Indonesia', 'alpha', '123456890001122', 'Karyawan', '01/02', 'Belum Kawin', '1999-03-12', 'indonesia'),
(14, 'Islam', 'Medan Baru', 'O', 'Laki-laki', 'Medan kota', 'medan baru', 'Indonesia', 'foxtrot', '1011123456732221', 'Karyawan', '01/02', 'Kawin', '1988-04-11', 'medan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `karyawan_master`
--
ALTER TABLE `karyawan_master`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
