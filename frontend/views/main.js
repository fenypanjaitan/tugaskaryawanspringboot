const url = "http://localhost:8080/"
var detail = 'http://127.0.0.1:5501/views/detailKaryawan.html?id'

function getData(url) {
    let xmlhttp = new XMLHttpRequest()
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let karyawans = JSON.parse(this.responseText)            //JSON.parse mengubah dari string ke json
            let karyawan = " "
            for (let i = 0; i < karyawans.length; i++) {
                karyawan += `<tr><td><a href="detailKaryawan.html?id=`+karyawans[i].id+`">`+karyawans[i].nama+`</a></td></tr>`
            }
            document.getElementById("namaKaryawan").innerHTML = karyawan
        }
    }
    xmlhttp.open("GET", url, true)
    xmlhttp.send()
}

function getItemById(id) {
    fetch(`http://localhost:8080/karyawans/`+id)
        .then(response => response.json())
        .then(data => renderItemsById(data))
        .catch(console.error);
            
}

const renderItemsById = (karyawans) =>{
    let karyawan = `
        <tr><td> NIK </td><td>:</td> <td> `+ karyawans.nik+`</td></tr><tr><td> Nama </td><td>:</td> <td>`+karyawans.nama+` 
        </td></tr><tr><td> Tempat,tanggal lahir </td><td>:</td> <td>`+karyawans.tempatlahir+`,`+karyawans.tanggallahir+`
        </td></tr><tr><td> Jenis Kelamin </td><td>:</td> <td>`+karyawans.jeniskelamin+`
        </td></tr><tr><td> Golongan Darah </td><td>:</td> <td>`+karyawans.goldarah+`
        </td></tr><tr><td> Alamat </td><td>:</td> <td>`+karyawans.alamat +` Rt/Rw `+karyawans.rtrw+` `+karyawans.keldesa+`
        </td></tr><tr><td> Kecamatan </td><td>:</td> <td>`+karyawans.kecamatan+`
        </td></tr><tr><td> Agama </td><td>:</td> <td>`+karyawans.agama+`
        </td></tr><tr><td> Status Perkawinan </td><td>:</td> <td>`+karyawans.statusperkawinan+`
        </td></tr><tr><td> Pekerjaan </td><td>:</td> <td>`+karyawans.pekerjaan+`
        </td></tr><tr><td> Kewarganegaraan </td><td>:</td> <td>`+karyawans.kewarganegaraan+`
        </td></tr> <br> <br>
        <button onclick="deleteId(`+karyawans.id+`)" class="btn btn-danger btn-fill pull-right id="hapus"  style="margin-left: 30px;">Hapus</button>
        <a href="editData.html?id=`+karyawans.id+`"><input type="button" class="btn btn-warning btn-fill" id="Edit" value="Edit"></a>
    `
    document.getElementById("detailKaryawan").innerHTML = karyawan

}

function deleteId (id) {
    fetch(`${url}/delete/${id}`, {
        method: "DELETE",
    })
   
    window.location.href = "listData.html"
};
