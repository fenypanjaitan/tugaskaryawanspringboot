const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const idParams = urlParams.get('id')
console.log(idParams);

function getItemById() {
    fetch(`http://localhost:8080/karyawans/` + idParams)
        .then(response => response.json())
        .then(data => renderItemsById(data))
        .catch(console.error);
}
const renderItemsById = (data) => {
    let karyawan = `
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Data</h4>
                    </div>
                    <div class="card-body">
                        <form method="POST" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>NIK</label>
                                        <input type="text" id="nik" class="form-control" placeholder="NIK" value="${data.nik}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" id="nama" class="form-control" placeholder="Nama" value="${data.nama}" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 pr-1">
                                    <div class="form-group">
                                        <label>Tempat Lahir</label>
                                        <input type="text" id="tempatlahir" class="form-control" placeholder="Tempat Lahir" value="${data.tempatlahir}" >
                                    </div>
                                </div>
                                <div class="col-md-6 pl-1">
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="date" id="tanggallahir" class="form-control" placeholder="Tanggal Lahir" value="${data.tanggallahir}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <select class="form-control" id="gender" value="${data.gender}">
                                            <option>--pilih--</option>
                                            <option>Laki-laki</option>
                                            <option>Perempuan</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Gol. Darah</label>
                                        <select class="form-control" id="goldarah" value="${data.goldarah}">
                                            <option>-</option>
                                            <option>A</option>
                                            <option>B</option>
                                            <option>AB</option>
                                            <option>O</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input type="text" id="alamat" class="form-control" placeholder="Alamat" value="${data.alamat}" >
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 pr-1">
                                    <div class="form-group">
                                        <label>RT/RW</label>
                                        <input type="text" id="rtrw" class="form-control" placeholder="rt/rw" value="${data.rtrw}">
                                    </div>
                                </div>
                                <div class="col-md-4 pl-1">
                                    <div class="form-group">
                                        <label>Kelurahan/Desa </label>
                                        <input type="text" id="keldesa" class="form-control" placeholder="Kelurahan/Desa" value="${data.keldesa}">
                                    </div>
                                </div>
                                <div class="col-md-5 pr-1">
                                    <div class="form-group">
                                        <label>Kecamatan</label>
                                        <input type="text" id="kecamatan" class="form-control" placeholder="Kecamatan" value="${data.kecamatan}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Agama</label>
                                        <select class="form-control" id="agama" value="${data.agama}">
                                            <option>--pilih--</option>
                                            <option>Islam</option>
                                            <option>Kristen</option>
                                            <option>Katholik</option>
                                            <option>Hindu</option>
                                            <option>Budha</option>
                                            <option>Konghuchu</option>
                                            <option>Ateis</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Status Perkawinan</label>
                                        <select class="form-control" id="status" value="${data.status}">
                                            <option>--pilih--</option>
                                            <option>Belum Kawin</option>
                                            <option>Kawin</option>
                                          </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Pekerjaan</label>
                                        <input type="text" id="pekerjaan" class="form-control" placeholder="pekerjaan" value="${data.pekerjaan}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Kewarganegaraan</label>
                                        <input type="text" id="kwn" class="form-control" placeholder="kewarganegaraan" value="${data.kewarganegaraan}">
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-info btn-fill pull-right"" onclick="update()" id="kirim" type="submit" name="action">Update Data</button>
                            
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
            
    
    `
    document.getElementById("content").innerHTML = karyawan
}

function update(id) {
    var nik = document.getElementById("nik").value;
        var nama = document.getElementById("nama").value;
        var tempatlahir = document.getElementById("tempatlahir").value;
        var tanggallahir = document.getElementById("tanggallahir").value;
        var jeniskelamin = document.getElementById("gender").value;
        var goldarah = document.getElementById("goldarah").value;
        var alamat = document.getElementById("alamat").value;
        var rtrw = document.getElementById("rtrw").value;
        var keldesa = document.getElementById("keldesa").value;
        var kecamatan = document.getElementById("kecamatan").value;
        var agama = document.getElementById("agama").value;
        var statusperkawinan = document.getElementById("status").value;
        var pekerjaan = document.getElementById("pekerjaan").value;
        var kewarganegaraan = document.getElementById("kwn").value;


    var str_json = JSON.stringify({
        nik: nik,
        nama: nama,
        tempatlahir:tempatlahir,
        tanggallahir:tanggallahir,
        jeniskelamin:jeniskelamin,
        goldarah:goldarah,
        alamat:alamat,
        rtrw:rtrw,
        keldesa:keldesa,
        kecamatan:kecamatan,
        agama:agama,
        statusperkawinan:statusperkawinan,
        pekerjaan:pekerjaan,
        kewarganegaraan:kewarganegaraan
    })
    console.log(str_json)
    fetch('http://localhost:8080/update/' + idParams, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: str_json,
        })
        // .then(response => response.json())
        .then(data => {
            alert("berhasil")
            console.log('Success:', data);
            window.location.href = "listData.html"
        })
        .catch((error) => {
            console.error('Error:', error);
        });
        window.location.href = "listData.html"
}

getItemById()